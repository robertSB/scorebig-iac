Configuration ContosoWebsite
{
	param ($MachineName)

	Node $MachineName
	{
		#Install the IIS Role
		WindowsFeature IIS
		{
			Ensure = �Present�
			Name = �Web-Server�
		}

		#Install ASP.NET 4.5
		WindowsFeature ASP
		{
			Ensure = �Present�
			Name = �Web-Asp-Net45�
		}

		WindowsFeature WebServerManagementConsole
		{
			Ensure = "Present"
			Name = "Web-Mgmt-Console"
		}

		WindowsFeature Web-Common-Http
		{
			Ensure = "Present"
			Name = "Web-Common-Http"
		}

		WindowsFeature Web-Performance
		{
			Ensure = "Present"
			Name = "Web-Performance"
		}

		WindowsFeature Web-Basic-Auth
		{
			Ensure = "Present"
			Name = "Web-Basic-Auth"
		}

		WindowsFeature Web-Windows-Auth
		{
			Ensure = "Present"
			Name = "Web-Windows-Auth"
		}

		WindowsFeature Web-Mgmt-Service
		{
			Ensure = "Present"
			Name = "Web-Mgmt-Service"
		}

		WindowsFeature Web-Scripting-Tools
		{
			Ensure = "Present"
			Name = "Web-Scripting-Tools"
		}

		WindowsFeature Web-Dyn-Compression
		{
			Ensure = "Present"
			Name = "Web-Dyn-Compression"
		}
		
	}
} 